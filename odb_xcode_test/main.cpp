//
//  main.cpp
//  odb_xcode_test
//
//  Created by jarm on 8/10/16.
//  Copyright © 2016 digb. All rights reserved.
//




#include <odb/result.hxx>
//#include <odb/odb.hxx>




#include <iostream>

#include <memory>   // std::auto_ptr
#include <iostream>

#include <odb/database.hxx>
#include <odb/transaction.hxx>

#include <odb/sqlite/database.hxx>

#include <odb/schema-catalog.hxx>// for gen schema

#include <odb/sqlite/simple-object-result.hxx>

#include "person.hpp"
#include "person-odb.hxx"


int main(int argc, char* argv[]) {
	try {
		std::auto_ptr<odb::database> db(new odb::sqlite::database("/Users/jarm/Desktop/odb_test"));
		
		// generate schema
		odb::transaction t(db->begin ());
		odb::schema_catalog::create_schema(*db);
		t.commit();
		
		unsigned long john_id, jane_id, joe_id;
		
		// Create a few persistent person objects.
		//
		{
			person john("John", "Doe", 33);
			person jane("Jane", "Doe", 32);
			person joe("Joe", "Dirt", 30);
			
			odb::transaction t(db->begin ());
			
			// Make objects persistent and save their ids for later use.
			//
			john_id = db->persist(john);
			jane_id = db->persist(jane);
			joe_id = db->persist(joe);
			
			t.commit();
		}
		
		{
			odb::transaction t(db->begin());
			
			odb::result<person> r(db->query<person>(odb::query<person>::age > 30));
			
			for (odb::result<person>::iterator i(r.begin()); i != r.end(); ++i) {
				std::cout << "Hello, " << i->first() << "!" << std::endl;
			}
			
			t.commit();
		}
	} catch (const odb::exception& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}
}



