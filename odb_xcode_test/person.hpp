//
//  person.hpp
//  odb_xcode_test
//
//  Created by jarm on 8/10/16.
//  Copyright © 2016 digb. All rights reserved.
//

#ifndef person_hpp
#define person_hpp

#include <stdio.h>

#include <string>

#include <string>

#include <odb/core.hxx>     // (1)

#pragma db object           // (2)
class person {
public:
	person (std::string first,
			std::string last,
			unsigned short age) {
		first_ = first;
		last_ = last;
		age_ = age;
	}
	std::string first() {
		return first_;
	}
private:
	          // (3)
	person () {}
	
	friend class odb::access; // (4)
	
#pragma db id auto        // (5)
	unsigned long id_;        // (5)
	
	std::string first_;
	std::string last_;
	unsigned short age_;
};

#endif /* person_hpp */
